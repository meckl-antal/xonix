from multiprocessing.pool import ThreadPool as Pool
import random

import numpy
import copy

from block import Block


def generate_random_route(game_map, route=[]):
    """
    Generate a random valid route for a map state.
    Note: Map should have only WALLS and EMPTY blocks
    :param game_map: Game map, with a player
    :return: Dictionary of route details
    """
    game_map.reset_map()
    if len(route) == 0:
        route = []
    while 1:  # I like to live dangerously
        next_move = random.randint(1, 4)
        route.append(next_move)
        try:
            rt_len = game_map.dry_run(route)
            if rt_len == 0:
                route = route[:-1]
            if rt_len > 1:
                break
        except ValueError:
            route = route[:-3]

    return {'route': numpy.copy(route),
            'len': len(route),
            'map': copy.deepcopy(game_map),
            'fitness': game_map.player.chromosome.fitness}


def generate_solutions_concurrent(count, game_map):
    """
    Generate concurrent solutions using multi-threading
    :param count: How many solutions are needed
    :param game_map: Map of the game
    :return: List of solutions of size :count:
    """
    solutions = []
    pool_size = 8
    pool = Pool(pool_size)
    for i in range(0, count):
        solutions.append(pool.apply_async(generate_random_route, (copy.deepcopy(game_map),)).get())
    pool.close()
    pool.join()
    return solutions


def fill_map(blocks):
    """Fil the map"""
    space1 = 0
    space2 = 0

    F1 = 9
    F2 = 8

    for rix, row in enumerate(blocks):
        current_color = F1
        for elx, element in enumerate(row):
            current_block = blocks[rix][elx]
            if current_block == Block.EMPTY:
                if current_color == F1:
                    space1 = space1 + 1
                else:
                    space2 = space2 + 1

                blocks[rix][elx] = current_color
            elif current_block == Block.PLAYER:
                space1 = space1 + 1
                space2 = space2 + 1
                current_color = F1 if current_color == F2 else F2
    return blocks


def is_on_map(size_x, size_y, x, y):
    """
    Returns if given coordinates are on the map.
    :param x: X coordinate
    :param size_x: X size of the map
    :param size_y: Y size of the map
    :param y: Y coordinate
    :return: True if the coordinates are on the map, False otherwise.
    """
    if x < 0 or x >= size_x:
        return False
    if y < 0 or y >= size_y:
        return False
    return True
