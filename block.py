class Block(object):
    """
    Class storing Block constants.
    """
    WALL = 1
    PLAYER = 2
    EMPTY = 0
    BALL = 4

    DIR_UP = 5
    DIR_DOWN = 6
    DIR_RIGHT = 7
    DIR_LEFT = 8

    PL_DIRS = [DIR_DOWN, DIR_RIGHT, DIR_LEFT, DIR_UP, PLAYER]
