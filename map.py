import time

import matplotlib.pyplot as plt
import numpy

from block import Block
from utils import is_on_map


class Map(object):
    def __init__(self, player, balls, size_x, size_y, blocks=[]):
        """
        Class representing the game map at a point in time
        :param player: The player on the map
        :param balls: The balls on the map
        :param size_x: Horizontal size of the map
        :param size_y: Vertical size of the map
        :param blocks: The blocks of the map (numpy matrix)
        """
        self.player = player
        self.player_original = [self.player.position_x, self.player.position_y]
        self.balls = balls
        self.size_x = size_x
        self.size_y = size_y
        self.blocks = blocks if len(blocks) else self.generate_map()
        self.clean = numpy.copy(self.blocks)

    def set_state(self, blocks, plx, ply):
        """
        Update the state of the map
        :param blocks: New blocks
        :param plx: Player's X position
        :param ply: Player's Y position
        :return: None
        """
        self.clean = blocks
        self.blocks = blocks
        self.player.position_x = plx
        self.player.position_y = ply

    def visualize_set(self, instructions):
        """
        Visualize a set of instructions.
        Each set is a list of instructions
        After each set the map is updated (filled)
        :param instructions: List of lists
        :return: None
        """
        fig = plt.figure()
        ax = fig.add_subplot(111)
        im = ax.imshow(self.blocks.tolist(), interpolation='nearest')
        im.set_cmap('gist_heat_r')
        plt.show(block=False)
        for instruction_set in instructions:
            empty_counter = 0
            for ins in instruction_set:
                try:
                    nb = self.move_player(direction=ins, empty_counter=empty_counter)
                    im.set_array(self.blocks.tolist())
                    if nb == Block.EMPTY:
                        empty_counter += 1
                    fig.canvas.draw()
                except ValueError:
                    pass

            new_map = []
            for block in self.blocks.flat:
                if block in Block.PL_DIRS:
                    new_map.append(Block.WALL)
                else:
                    new_map.append(block)

            reshaped = numpy.reshape(new_map, (-1, self.size_x))
            reshaped[self.player.position_y][self.player.position_x] = Block.PLAYER
            self.blocks = reshaped
            im.set_array(self.blocks.tolist())
            fig.canvas.draw()

        time.sleep(5)

    def dry_run(self, instructions):
        """
        Run and validate a set of instructions without visualizing them
        :param instructions: Array of integers
        :return: The index of the route until its valid to execute
        :raises: ValueError if route hits the trail
        """
        self.reset_map()
        self.player.chromosome.instructions = instructions
        empty_block_counter = 0

        for index, direction in enumerate(instructions):
            try:
                nb = self.move_player(direction=direction, empty_counter=empty_block_counter)
            except ValueError as ex:
                if ex.message == Block.WALL:
                    return index
                else:
                    raise ValueError(ex.message)
            else:
                if nb == Block.EMPTY:
                    empty_block_counter += 1
        return None

    def reset_map(self):
        """
        Reset the map to the state of creation.
        :return: None
        """
        self.blocks = numpy.copy(self.clean)
        self.player.chromosome.fitness = -1
        self.player.position_x = self.player_original[0]
        self.player.position_y = self.player_original[1]

    def visualize_chromosome(self, instructions):
        fig = plt.figure()
        ax = fig.add_subplot(111)
        im = ax.imshow(self.blocks.tolist(), interpolation='nearest')
        im.set_cmap('hot')
        plt.show(block=False)
        empty_counter = 0
        for direction in instructions:
            try:
                self.move_player(direction=direction, empty_counter=empty_counter)
                im.set_array(self.blocks.tolist())
                fig.canvas.draw()
            except ValueError:
                break

        time.sleep(1)

    def generate_map(self):
        """
        Generate initial map.
        Fill the sides with walls, fill others with empty block and place the player on the map.
        :return: Newly generated map
        """
        empty = numpy.zeros(shape=(self.size_y, self.size_x), dtype=object)
        for row_index, row in enumerate(empty):
            for column_index, column in enumerate(row):
                if (column_index == 0 or row_index == 0 or
                            row_index == self.size_y - 1 or column_index == self.size_x - 1):
                    empty[row_index][column_index] = Block.WALL
                else:
                    empty[row_index][column_index] = Block.EMPTY
        empty[self.player.position_y][self.player.position_x] = Block.PLAYER
        return empty

    def update_player_position(self, ppx, ppy, drs):
        """
        Set player position on the map.
        :param ppx: Player's X position
        :param ppy: Player's Y position
        :param drs: Direction where the player is heading
        :return: None
        """
        self.blocks[ppy][ppx] = Block.PLAYER
        self.blocks[self.player.position_y][self.player.position_x] = drs
        self.player.position_x = ppx
        self.player.position_y = ppy

    def move_player(self, direction, empty_counter):
        """
        Move player on map. Also validate movement.
        
        :param direction: The next direction where the player wants to move 
        :param empty_counter: How many empty blocks were touched in current iteration
        :return: 
        """
        next_block = Block.PLAYER

        if direction == 1:
            # DOWN
            ppx = self.player.position_x
            ppy = self.player.position_y + 1

            if not is_on_map(self.size_x, self.size_y, ppx, ppy):
                # If goes off the map
                raise ValueError(Block.PLAYER)

            next_block = self.blocks[ppy][ppx]
            if next_block == Block.WALL:
                if empty_counter > 2:
                    self.update_player_position(ppx, ppy, Block.DIR_DOWN)
                    raise ValueError(Block.WALL)
            if next_block in Block.PL_DIRS:
                raise ValueError(Block.PLAYER)

            self.update_player_position(ppx, ppy, Block.DIR_DOWN)

        if direction == 3:
            # TOP
            ppx = self.player.position_x
            ppy = self.player.position_y - 1

            if not is_on_map(self.size_x, self.size_y, ppx, ppy):
                # If goes off the map
                raise ValueError(Block.PLAYER)

            next_block = self.blocks[ppy][ppx]
            if next_block == Block.WALL:
                if empty_counter > 2:
                    self.update_player_position(ppx, ppy, Block.DIR_UP)
                    raise ValueError(Block.WALL)
            if next_block in Block.PL_DIRS:
                raise ValueError(Block.PLAYER)

            self.update_player_position(ppx, ppy, Block.DIR_UP)

        if direction == 2:
            # RIGHT
            ppx = self.player.position_x + 1
            ppy = self.player.position_y

            if not is_on_map(self.size_x, self.size_y, ppx, ppy):
                # If goes off the map
                raise ValueError(Block.PLAYER)

            next_block = self.blocks[ppy][ppx]
            if next_block == Block.WALL:
                if empty_counter > 2:
                    self.update_player_position(ppx, ppy, Block.DIR_RIGHT)
                    raise ValueError(Block.WALL)
            if next_block in Block.PL_DIRS:
                raise ValueError(Block.PLAYER)

            self.update_player_position(ppx, ppy, Block.DIR_RIGHT)

        if direction == 4:
            # LEFT
            ppx = self.player.position_x - 1
            ppy = self.player.position_y

            if not is_on_map(self.size_x, self.size_y, ppx, ppy):
                # If goes off the map
                raise ValueError(Block.PLAYER)

            next_block = self.blocks[ppy][ppx]
            if next_block == Block.WALL:
                if empty_counter > 2:
                    self.update_player_position(ppx, ppy, Block.DIR_LEFT)
                    raise ValueError(Block.WALL)
            if next_block in Block.PL_DIRS:
                raise ValueError(Block.PLAYER)

            self.update_player_position(ppx, ppy, Block.DIR_LEFT)
        return next_block

    def map_state_in_advance(self, nr_steps_ahead):
        """
        Get the state of the map in nr_steps_ahead
        :param nr_steps_ahead: How many steps ahead
        :return: Map object
        """
        raise NotImplementedError
