import random


class Chromosome(object):
    def __init__(self, player_instructions=None, fitness=-1):
        """
        Class for representing a chromosome, i.e a game state
        :param player_instructions: List of instructions for the player.
        :param fitness: Fitness value of the chromosome.
        """
        self.instructions = player_instructions
        self.fitness = fitness

    def mutate(self, probability):
        """
        Mutate a chromosome
        :param probability: Integer, between 0 and 100.
        :return: A new chromosome instance.
        """
        if probability <= random.randint(0, 100):
            return self.instructions[:-3]
        else:
            return self.instructions

    def __lt__(self, other):
        return other.fitness > self.fitness

    def __eq__(self, other):
        return other.fitness == self.fitness

    def __ne__(self, other):
        return not self.__eq__(other)
