class Ball(object):
    def __init__(self, position_x, position_y, direction):
        """
        Class for representing a ball
        :param position_x: X position on the map
        :param position_y: Y position on the map
        :param direction: Degree representing in which direction is heading
        """
        self.position_x = position_x
        self.position_y = position_y
        self.direction = direction
