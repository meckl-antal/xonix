import copy

import numpy

from block import Block
from utils import generate_solutions_concurrent, generate_random_route


class Game(object):
    def __init__(self, game_map, initial_population_size, nr_of_generations):
        """
        Class representing a game
        :param game_map: The initial map of the game
        :param initial_population_size: Population size
        :param nr_of_generations: Number of generations 
        """
        self.game_map = game_map
        self.clean_map = copy.deepcopy(game_map)
        self.initial_population = initial_population_size
        self.generations = nr_of_generations
        self.game_states = []

    def start(self, cycles=4):
        """
        Run a full game
        :param cycles: 
        :return: List of instruction sets, each individual instruction set is based on the current map
        and executes a wall-to-wall movement.
        """
        sets = []
        solution = self.select_next_path(self.game_map)
        for cycle in range(0, cycles):
            sets.append(solution['route'])  # Update solution set
            self.game_map.set_state(
                blocks=solution['new_map'],
                plx=solution['player_x'],
                ply=solution['player_y'])
            solution = self.select_next_path(self.game_map)

        self.clean_map.visualize_set(sets)
        return sets

    def select_next_path(self, game_map):
        """
        Returns the best next chromosome for the current map setup
        using genetic algorithm
        :param game_map: Game map
        :return: Dictionary consisting of run information
        """

        sol = generate_solutions_concurrent(count=self.initial_population, game_map=game_map)
        kill_count = self.initial_population / 3

        for _ in range(0, self.generations):
            # sort the population by fitness
            ranked = sorted(sol, key=lambda k: k['len'], reverse=True)

            # kill the the least fit
            after_kill = ranked[:-kill_count]

            sol = []
            for solution in after_kill:
                # mutate chromosomes
                mutated_instructions = solution['map'].player.chromosome.mutate(50)
                # fix mutated chromosome
                sol.append(generate_random_route(game_map, mutated_instructions))

            for died in range(0, kill_count):
                # generate random ones for the deceived
                game_map.reset_map()
                sol.append(generate_random_route(game_map))

        best_chromosome = sorted(sol, key=lambda k: k['len'], reverse=True)[0]
        plx = best_chromosome['map'].player.position_x
        ply = best_chromosome['map'].player.position_y
        flat_map = best_chromosome['map'].blocks.flat

        new_map = [cb if cb not in Block.PL_DIRS else Block.WALL for cb in flat_map]

        reshaped = numpy.reshape(new_map, (-1, best_chromosome['map'].size_x))
        reshaped[ply][plx] = Block.PLAYER

        return {
            'new_map': reshaped,
            'player_x': best_chromosome['map'].player.position_x,
            'player_y': best_chromosome['map'].player.position_y,
            'route': best_chromosome['route'].tolist()
        }
