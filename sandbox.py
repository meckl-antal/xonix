from game import Game
from map import Map
from player import Player

if __name__ == '__main__':

    player = Player(position_x=0, position_y=0)
    game_map = Map(size_x=13, size_y=20, player=player, balls=None)
    game = Game(game_map, initial_population_size=10, nr_of_generations=7)
    game.start(cycles=7)
