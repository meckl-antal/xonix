from chromosome import Chromosome


class Player(object):
    def __init__(self, position_x, position_y, trail=None):
        """
        Class for representing a player
        :param position_x: X position on the map
        :param position_y: Y position on the map
        :param trail: List of Block objects, representing the current trail of the player
        this value resets when the player reaches the wall
        """
        self.position_x = position_x
        self.position_y = position_y
        self.trail = trail
        self.chromosome = Chromosome()

    def compute_fitness(self, area_map):
        self.chromosome.fitness = area_map.sum()

    def get_fitness(self, arr, start_x, start_y, map_x, map_y):
        self.chromosome.fitness = 3
