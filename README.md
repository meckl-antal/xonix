# Xonix Evolve


Automated Xonix gameplay using *genetic algorithm*.


## Requirements
1. python 2.X
2. python-virtualenv

## Installation
1. Create a new python virtualenv
 * virtualenv <name_of_the_env>
2. Activate environment
 * source path/to/environment/created/bin/activate
3. Install requirements
 * cd path/to/project
 * pip install -r *requirements.txt*

## Test the application
Tweak the parameters in *sandbox.py* and run using

`
python sandbox.py
`